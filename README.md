
# Sort API
  
Sort api é para usuarios que desejam ordernar uma lista de coisas.

## Quick Start

- Instalar as depedencias:

```

$ npm install

```
- Iniciar o servidor


```

$ npm start

```

  

## Testes

  

Para rodar os testes, basta utilizar o codigo

  

```

$ npm test

```

  

## API


|Name|Endpoint  | Method  | Params|  Return | 
|--|--|--|--|--|
|[Sort](#sort)|/api/v1/sort|GET|Object|Object|




### Sort
Metodo para ordernar uma lista de um ou varios objetos.

#### Parametro
```
	Object:{
		items: Array<String | number>,
		asc?: boolean //Default is true
	}
```

#### Retorno
```
	Object:{
		Object: Array <String | number>
		...
	}
```

##### Exemplo
```js
    const lista = {
	    comidas: {
			items:["Batata","Arroz","Feijão"],
			asc:  true
		},
		paises: {
			items:["Brasil","Portugal","Argentina"],
			asc:  false
		},
	};
	
	fetch(`http://localhost:3000/api/v1/sort?list=${JSON.stringfy(lista)}`, {method: 'get'})
		.then(function(response) {
			console.log(response); // { comidas: ["Arroz, "Batata, "Feijão"], paises: ["Portugal","Brasil","Argentina"] }
			//..algum codigo
		})
```


## Autor

* **Anderson Fernandes** -  [github](https://github.com/AndersonFeernandes)
