module.exports = {
	ignoreSpecialChars:function(word){
		if(!word)return word;
		if(!isNaN(word))return word;
	
		word = word.replace(/á/ig, "a"); 
		word = word.replace(/â/ig, "a"); 
		word = word.replace(/ã/ig, "a"); 
		word = word.replace(/à/ig, "a"); 


		word = word.replace(/é/ig, "e"); 
		word = word.replace(/ê/ig, "e"); 
		word = word.replace(/è/ig, "e"); 


		word = word.replace(/í/ig, "i"); 
		word = word.replace(/î/ig, "i"); 
		word = word.replace(/ì/ig, "i"); 


		word = word.replace(/ó/ig, "o"); 
		word = word.replace(/ô/ig, "o"); 
		word = word.replace(/õ/ig, "o"); 
		word = word.replace(/ò/ig, "o"); 

		word = word.replace(/ú/ig, "u"); 
		word = word.replace(/û/ig, "u"); 
		word = word.replace(/ù/ig, "u"); 


		return word.toLowerCase();
	},
	

  insertionSort: function (arr, asc = true) {
    for (let i = 0; i < arr.length; i++) {
	    let item = arr[i]
	    let tempIndex;
	    for (tempIndex = i - 1; this.ignoreSpecialChars(arr[tempIndex]) > this.ignoreSpecialChars(item); tempIndex--) {
	    	if(arr[tempIndex]){
	   			 arr[tempIndex + 1] = arr[tempIndex]
	   		}
	    }
	    arr[tempIndex + 1] = item
	  }
	  if(!asc){
	  	arr.reverse();
		}
	  return arr;
	},


};
