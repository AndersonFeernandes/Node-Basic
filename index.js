const express = require('express');
const app = express();
const port = 3000;
const sort = require('./sortArray');

app.get('/api/v1/sort', function(req, res) { 
	try{
		let paramsSort = {};
		let lists = JSON.parse(req.query.list);
		
		if(typeof lists == 'string')throw new Error("Null list");

		for( let key in lists){
			let list = lists[key];
			let itemsSort = sort.insertionSort(list.items,list.asc);
			paramsSort[key] = itemsSort;

		}

		res.json(paramsSort);
	}catch(err){
		res.status(400).json({error: "Invalid List", tracking: err});
	}
});

app.listen(port, () => {
	console.log(`Server running on port: ${port}`);
});


module.exports = app;