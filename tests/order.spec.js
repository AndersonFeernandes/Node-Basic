const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../index');
const should = chai.should();
const _API_URL = "/api/v1";

chai.use(chaiHttp);

describe("/GET sort",() => {
    it('Deve me retornar a lista ordernada', (done) => {
        let lista = {
            comidas: {
                items:["Batata","Arroz","Feijão"], 
                asc: true	
            },
            paises: {
                items:["Brasil","Portugal","Argentina"], 
                asc: false	
            },
            animais: {
                items:["Hamster","Cachorro","Gato","Águia"]
            },
            nomes: {
                items: ["Anderson Luiz", "Anderson Luíz", "anderson Fernandes"],
            },
            salas: {
                items: ['11 A',"21 B",'31 C', '32 C', '12 A','22 B'],
                asc: true
            }
        };

        const listaOrdenada = {
            comidas: ["Arroz","Batata","Feijão"],
            paises: ["Portugal","Brasil","Argentina"], 
            animais:["Águia","Cachorro","Gato","Hamster"],
            nomes: ["anderson Fernandes","Anderson Luiz", "Anderson Luíz"],
            salas: ["11 A","12 A","21 B","22 B","31 C","32 C"]
        };
           
        chai.request(server).get(`${_API_URL}/sort?list=${JSON.stringify(lista)}`).end((err, res)=>{
            res.should.have.status(200);
            res.body.should.be.a('Object');
            res.body.should.have.property('comidas')
            res.body.should.have.property('paises');
            res.body.should.have.property('animais');
            res.body.should.have.property('nomes');
            res.body.should.have.property('salas');

            res.body.should.to.eql(listaOrdenada);
            done();

        });

    })


    it('Deve me retornar erro por mandar parametro errado', (done) => {
        const fakeParam = "parametro incorreto";

           
        chai.request(server).get(`${_API_URL}/sort?list=${fakeParam}`).end((err, res)=>{
            res.should.have.status(400);
            done();
        });

    })

    it('Deve me retornar erro por mandar parametro vazio', (done) => {
        const fakeParam = "";

           
        chai.request(server).get(`${_API_URL}/sort?list=${fakeParam}`).end((err, res)=>{
            res.should.have.status(400);
            done();
        });

    })

    it('Deve me retornar erro por mandar objeto função como parâmetro', (done) => {
        const funcComida = function(comida){
            console.log(comida);
        }

        const fakeParam = { comidas: ['Macarrão', 'Arroz', 'Pipoca',funcComida] };

        chai.request(server).get(`${_API_URL}/sort?list=${JSON.stringify(fakeParam)}`).end((err, res)=>{
            res.should.have.status(400);
            done();
        });


    })
    
})